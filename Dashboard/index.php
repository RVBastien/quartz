<?php session_start(); ?>
<?php if(!isset($_SESSION['login'])){$_SESSION['login'] = false;} ?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.1.2/firebase.js"></script>
    <script src="app/js/connect.js"></script>
    <title>DashBoard</title>

</head>
<body>
<?php if ($_SESSION['login'] === false) { ?>
    <div id="login">
        Se connecter via firebase
    </div>
<?php } else { ?>
    <header>
        <nav>
            <ul>
                <li><a href="preprod.php">Preproduction</a></li>
            </ul>
        </nav>
    </header>
    <div id="deco">
        Se déconnecter
    </div>
    <script type="application/ecmascript">
        $("#deco").click(function () {
            $.post("connect.php",
                {
                    statut: false
                }, function () {
                    location.reload();
                });
        });
    </script>
<?php } ?>

<script>
    firebase.initializeApp(config);
    firebase.auth().onAuthStateChanged(function (user) {

        /* Limitation de l'usage du bouton de connection -> Connection -> Récupération des données utilisateurs -> Message de bienvenue */
        let debounce = true;

        if (debounce) {
            $("#login").click(function () {
                let provider = new firebase.auth.GoogleAuthProvider();
                firebase.auth().signInWithPopup(provider).then(function (result) {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    let token = result.credential.accessToken;
                    // The signed-in user info.
                    let user = result.user;

                    if (user.uid === 'xHdFCO5ji1ddSDno7Wi1tQiIy4S2') {
                        $.post("connect.php",
                            {
                                statut: true
                            }, function () {
                                location.reload();
                            });
                    }
                    debounce = false;
                    setTimeout(function () {
                        debounce = true;
                    }, 1000);
                    // ...
                }).catch(function (error) {
                    // Handle Errors here.
                    let errorCode = error.code;
                    let errorMessage = error.message;
                    // The email of the user's account used.
                    let email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    let credential = error.credential;
                    // ...
                    console.log(errorMessage);
                });

            });
        }
    });
</script>
</body>
</html>