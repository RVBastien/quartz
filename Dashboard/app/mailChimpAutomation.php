<?php
function mc_request($api, $type, $target, $data = false)
{
    $ch = curl_init($api['url'] . $target);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array
    (
        'Content-Type: application/json',
        'Authorization: ' . $api['login'] . ' ' . $api['key'],
//		'X-HTTP-Method-Override: ' . $type,
    ));

//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT');

    if ($data)
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

$mailChimpTargetedList = array('url'=> 'https://us16.api.mailchimp.com/3.0/','login'=>'HushApp','key'=>'de85102d480881682fdb0d4dd62e1934-us16');

$email = $_POST['email'];

$addRequest = '{"email_address":"'.$email.'", "status":"subscribed"}';

mc_request($mailChimpTargetedList,'POST','lists/baa0dffa5c/members',$addRequest);