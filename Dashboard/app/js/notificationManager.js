///// PARAM /////

let lockedTimer = 60000;
let path = "app";

//////////////////////////////////// Historic generation function  //////////////////////////////

let notificationManager = function (TargetedUserUid, SenderUserId, smileyType, catchphrase, histoCatchphrase) {
    let userUid = cookieLib().userId;
    let notifSend = {};
    let notifStatus = {};
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let seconds = today.getSeconds();
    let minutes = today.getMinutes();
    let hour = today.getHours();
    let yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    mm = parseInt(mm);

    let todarray = [seconds, minutes, hour, mm, dd, yyyy];
    firebase.database().ref('user_profil/' + userUid).once('value').then(function (snapshot) {
        let senderName = snapshot.val().userName;
        let senderProfilPict = snapshot.val().profilPicture;

        notifSend = {
            notificationIs: smileyType,
            catchphrase: catchphrase,
            senderName: senderName,
            senderProfilPict: senderProfilPict,
            histoCatchphrase: histoCatchphrase,
            date: todarray
        };

        notifStatus = {
            status: "pending",
            notificationIs: smileyType,
            catchphrase: catchphrase,
            senderName: senderName,
            senderProfilPict: senderProfilPict,
            histoCatchphrase: histoCatchphrase,
            date: todarray,
            waiting: "true"
        };
        // update recipient
        firebase.database().ref('user_profil/' + TargetedUserUid + "/notificationReceived/" + SenderUserId).set(notifSend);
        //update sender
        firebase.database().ref('user_profil/' + SenderUserId + "/notificationSent/" + TargetedUserUid).set(notifStatus);
    });
};

//////////////////////////////////// Sending Trigger  //////////////////////////////

$("#emoji-1").on('click', function () {
    let targetedUserId = $("#home-picture").attr("class");
    let userUid = cookieLib().userId;
    return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
        let sentNotification = snapshot.val() || {status: 'null'};
        firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
            let friendTest = snapshot.val();
            if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                $('#overlay-notifs').css('display', 'none');
                // You anti Spam message goes here
                $('.toMuchHush').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush').fadeOut(1500);
                    }, 1000);
                });
            } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                $('.toMuchHush2').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush2').fadeOut(1500);
                    }, 3000);
                });
            } else {
                notificationManager(targetedUserId, userUid, "img/notification/smoke.png", "Vous invite à fumer", "de fumé avec vous");
            }
        });
    });
});
$("#emoji-2").click(function () {
    let targetedUserId = $("#home-picture").attr("class");
    let userUid = cookieLib().userId;
    return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
        let sentNotification = snapshot.val() || {status: 'null'};
        firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
            let friendTest = snapshot.val();
            if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                $('#overlay-notifs').css('display', 'none');
                // You anti Spam message goes here
                $('.toMuchHush').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush').fadeOut(1500);
                    }, 1000);
                });
            } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                $('.toMuchHush2').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush2').fadeOut(1500);
                    }, 3000);
                });
            } else {
                notificationManager(targetedUserId, userUid, "img/notification/dance.png", "Vous invite à danser", "de dansé avec vous");
            }
        });
    });
});
$("#emoji-3").click(function () {
    let targetedUserId = $("#home-picture").attr("class");
    let userUid = cookieLib().userId;
    return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
        let sentNotification = snapshot.val() || {status: 'null'};
        firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
            let friendTest = snapshot.val();
            if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                $('#overlay-notifs').css('display', 'none');
                // You anti Spam message goes here
                $('.toMuchHush').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush').fadeOut(1500);
                    }, 1000);
                });
            } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                $('.toMuchHush2').fadeIn(200, function () {
                    setTimeout(function () {
                        $('.toMuchHush2').fadeOut(1500);
                    }, 3000);
                });
            } else {
                notificationManager(targetedUserId, userUid, "img/notification/drink.png", "Vous invite à boire", "de boire avec vous");
            }
        });
    });
});

function createHisto(requestId, state, senderName, histoCatchphrase, profilPict, dateOfNotif, status, smileyType, catchphrase, senderId) {
    firebase.database().ref('user_profil/' + requestId + '/notificationHistoric/' + state).push({
            senderName: senderName,
            catchphrase: catchphrase,
            histoCatchphrase: histoCatchphrase,
            profilPict: profilPict,
            notificationIs: smileyType,
            dateOfNotif: dateOfNotif,
            status: status,
            senderId: senderId,
            startedAt: firebase.database.ServerValue.TIMESTAMP
        }
    );
}


//////////////////////////////////// Invitation window pop-in  //////////////////////////

$(function () {
    let userUid = cookieLib().userId;

    // Notif recieved listenner
    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationReceived/').on('value', function (snapshot) {
        if (snapshot.val() !== null) {
            $(".overlay-guest").remove();
            for (let sender in snapshot.val()) {

                firebase.database().ref('user_profil/' + userUid + '/' + 'notificationReceived/' + sender).once('value', function (snapshot) {
                    let receivedNotification = snapshot.val();
                    if (receivedNotification !== null) {
                        if (receivedNotification.status !== "seen" && receivedNotification.status !== "refused" && receivedNotification.waiting !== "true") {
                            let generatedFormattedId = "#" + sender;
                            let today = new Date();
                            let dd = today.getDate();
                            let mm = today.getMonth() + 1; //January is 0!
                            let seconds = today.getSeconds();
                            let minutes = today.getMinutes();
                            let hour = today.getHours();
                            let yyyy = today.getFullYear();
                            let todarray = [seconds, minutes, hour, mm, dd, yyyy];
                            let notifDate = receivedNotification.date;
                            let timestamp = [];
                            timestamp[0] = todarray[0] - notifDate[0];
                            timestamp[1] = todarray[1] - notifDate[1];
                            timestamp[2] = todarray[2] - notifDate[2];
                            timestamp[3] = todarray[3] - notifDate[3];
                            timestamp[4] = todarray[4] - notifDate[4];
                            timestamp[5] = todarray[5] - notifDate[5];

                            let resultTime;

                            if (timestamp[0] !== 0) {
                                resultTime = "À l'instant";
                                if (timestamp[1] > 0) {
                                    resultTime = "Il y a " + Math.abs(timestamp[1]) + " minutes ";
                                    if (timestamp[3] !== 0) {
                                        resultTime = Math.abs(timestamp[3]) + " heures " + resultTime;
                                        if (timestamp[2] !== 0) {
                                            resultTime = Math.abs(timestamp[2]) + " jours " + resultTime;
                                            if (timestamp[4] !== 0) {
                                                resultTime = Math.abs(timestamp[4]) + " mois " + resultTime;
                                                if (timestamp[5] !== 0) {
                                                    resultTime = Math.abs(timestamp[5]) + " ans " + resultTime;
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                            $(".target-notification").append('<div id="' + sender + '" class="overlay-guest"><div class="overlay-guest-content"><div class="pictureProfil"><figure class="picture-profil-guest" style="background-image:url(' + receivedNotification.senderProfilPict + ')"></figure></div><div class="guest-title"> <p class="content-guest-title">' + receivedNotification.senderName + '</p> <p><span></span>' + receivedNotification.catchphrase + '</p> </div><figure class="overlay-guest-contentEmoji"><img src="' + receivedNotification.notificationIs + '" width="70"/></figure><div class="optionsGuest"><div id="' + sender + 'y' + '" class="btn-guest-yes btn-cta-pink">Accepter</div> <div id="' + sender + 'n' + '" class="btn-guest-no btn-cta-black">Refuser</div></div><div id="' + sender + 'p' + '" class="btn-cta-close"><img src="img/pictos/close.svg" width="20" height="auto"/></div> </div> </div>');

                            if (document.getElementById(sender + 'h') === null) {
                                $(".notifsPending").prepend('<div id="' + sender + 'h" class="notifications-content"> <figure style="background: url(' + receivedNotification.senderProfilPict + ')"> </figure> <div class="notifications-content-informations"> <p class="notifications-name">' + receivedNotification.senderName + '</p> <p class="notifications-action">' + receivedNotification.catchphrase + '</p> <p class="notifications-timeline">' + resultTime + '</p> </div> <img src="' + receivedNotification.notificationIs + '" width="40" height="auto"> <div class="toggleNotifications"> <div class="toggleNotifications-content"> <div id="' + sender + 'yh' + '" class="btn-cta-pink notif-yes">ACCEPTER</div> <div id="' + sender + 'nh' + '" class="btn-cta-black notif-no">REFUSER</div> </div> </div> </div>');
                            }

                            let yesTrigger = generatedFormattedId + 'y';

                            $(yesTrigger).click(function () {
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "keep"});
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({waiting: "false"});
                                $(generatedFormattedId).remove();
                                $(generatedFormattedId + "h").remove();
                            });

                            let yesTriggerH = generatedFormattedId + 'yh';

                            $(yesTriggerH).click(function () {
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "keep"});
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({waiting: "false"});
                                $(generatedFormattedId).remove();
                                $(generatedFormattedId + "h").remove();
                            });

                            let noTrigger = generatedFormattedId + 'n';

                            $(noTrigger).click(function () {
                                $(generatedFormattedId).remove();
                                $(generatedFormattedId + "h").remove();
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "refused"});
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({waiting: "false"});
                            });

                            let noTriggerHist = generatedFormattedId + 'nh';

                            $(noTriggerHist).click(function () {
                                $(generatedFormattedId).remove();
                                $(generatedFormattedId + "h").remove();
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "refused"});
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({waiting: "false"});
                            });

                            let passTrigger = generatedFormattedId + 'p';

                            $(passTrigger).click(function () {
                                $(generatedFormattedId).remove();
                                $(generatedFormattedId + "h").remove();
                                firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({
                                    waiting: "true",
                                    status: "seen"
                                });
                            });
                            Push.create('' + snapshot.val().senderName + ' vous a hushé ! ', {
                                body: snapshot.val().catchphrase,
                                icon: snapshot.val().notificationIs,
                                link:path
                            });
                        }
                    }
                });
            }
        }
    });

    //////////////////////////////////// Historic generator and notification pushing //////////////////////////

    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/').on('value', function (snapshot) {
        // le status des demandes a changé
        for (let requestId in snapshot.val()) {
            firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).once('value', function (snapshot) {

                let statuts = snapshot.val().status;
                let waiting = snapshot.val().waiting;
                if (statuts !== null && statuts !== "pending" && statuts !== "done") {
                    firebase.database().ref('user_profil/' + requestId).once('value').then(function (snapshot) {
                        let userName = snapshot.val().userName;
                        let lastName = snapshot.val().lastName;
                        let age = snapshot.val().age;
                        let description = snapshot.val().description;
                        let profilPict = snapshot.val().profilPicture;
                        let userRelationShip = snapshot.val().relationship;
                        let ghostMod = snapshot.val().ghostMod;
                        let formattedIdFriends = requestId + "f";
                        firebase.database().ref('user_profil/' + requestId + '/notificationReceived/' + userUid).once('value').then(function (snapshot) {
                            let userSendedPict = snapshot.val();
                            let fetchVar = '/notificationReceived/';
                            if (userSendedPict !== null || userSendedPict === null && waiting !== "true") {
                                fetchVar = '/' + 'notificationSent/';
                                firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).once('value', function (snapshot) {
                                    userSendedPict = snapshot.val();
                                });
                            }
                            if (userSendedPict !== null) {

                                userSendedPict = userSendedPict.notificationIs;

                                if (statuts === "accepted" || statuts === "keep" || statuts === "reset") {
                                    if (waiting !== "true" && statuts !== "reset") {
                                        $(".target-notification").append('<div class="overlay-guest-yes"><div class="overlay-guest-pictureEmoji"><figure class="overlay-profil-guest" style="background-image:url(' + profilPict + ')"></figure> <figure class="emoji-send-guest"><img src="' + userSendedPict + '" alt="Request No"/> </figure></div> <div class="overlay-guest-title"> <p><span>' + userName + '</span></p> <p>a <span class="request-yes">accepté</span> votre demande !</p> </div> </div>');
                                        let friendVerif;
                                        firebase.database().ref('user_profil/' + userUid + '/friendList/' + requestId).once("value", function (snapshot) {
                                            friendVerif = snapshot.val();
                                            if (document.getElementById(formattedIdFriends) === null && friendVerif === null) {
                                                $("#item-friends").addClass("superActive");
                                                $(".friendsPending").prepend('<div id="' + formattedIdFriends + '" class="notifications-content"> <figure style="background-image:url(' + profilPict + ')"></figure> <div class="notifications-content-informations"> <p class="notifications-name">' + userName + '</p> <p class="notifications-action">' + userRelationShip + '</p><div class="viewProfil"></div></div></div></div>');
                                                formattedIdFriends = "#" + formattedIdFriends;
                                                $(formattedIdFriends).click(function () {
                                                    $(".profilPictGuest").css("background", "url('" + profilPict + "')");
                                                    $("#firstNameGuest").html(userName);
                                                    $("#lastNameGuest").html(lastName);
                                                    $("#ageGuest").html(age);
                                                    $("#descriptionGuest").html(description);
                                                    $(".remove").hide();
                                                    $(".add").show();
                                                    if (ghostMod === "off") {
                                                        $(".profil-pop-in .profilStatus").show();
                                                        $(".relationStatusGuest").html(userRelationShip);
                                                    } else {
                                                        $(".profil-pop-in .profilStatus").hide();
                                                    }
                                                    $(".quit").click(function () {
                                                        $(".profil-pop-in").fadeOut();
                                                    });
                                                    $(".add").attr("id", requestId + "add");
                                                    let addId = "#" + requestId + "add";
                                                    $(addId).click(function () {
                                                        if (firebase.database().ref('user_profil/' + userUid + '/friendList/' + requestId).once("value", function (snapshot) {
                                                                if (snapshot.val() === null) {
                                                                    firebase.database().ref('user_profil/' + requestId + '/friendList/' + userUid).set({
                                                                        group: "none"
                                                                    });
                                                                    firebase.database().ref('user_profil/' + userUid + '/friendList/' + requestId).set({
                                                                        group: "none",
                                                                        notifStatus: "toDo"
                                                                    });
                                                                }
                                                            }))
                                                            $(formattedIdFriends).remove();
                                                        $(".profil-pop-in").fadeOut();
                                                    });
                                                    $(".profil-pop-in").fadeIn();
                                                });
                                            }
                                        });
                                        $(".overlay-guest-yes").show().animate({'top': '0px'}, 400).delay(1000).fadeOut(1000, function () {
                                            $(this).remove();
                                        });
                                        Push.create('' + userName + ' a accepté le hush ! ', {
                                            body: "Votre demande a été acceptée !",
                                            icon: profilPict,
                                            link:path
                                        });

                                        firebase.database().ref('user_profil/' + userUid + fetchVar + requestId).once("value", function (snapshot) {
                                            let requestStored = snapshot.val();
                                            let senderName = requestStored.senderName;
                                            let histoCatchphrase = requestStored.histoCatchphrase;
                                            let catchPhrase = requestStored.catchphrase;
                                            let smileyType = userSendedPict;
                                            let profilPict = requestStored.senderProfilPict;
                                            let dateOfNotif = requestStored.date;
                                            if (waiting !== "true") {
                                                createHisto(requestId, "done", senderName, histoCatchphrase, profilPict, dateOfNotif, "a accepté", smileyType, catchPhrase, requestId);
                                            }
                                        });

                                        firebase.database().ref('user_profil/' + requestId + '/notificationReceived/').child(userUid).remove();
                                        firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).update({status: "keep"});
                                        firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).update({waiting: "true"});
                                    }
                                } else if (statuts === "seen") {
                                    firebase.database().ref('user_profil/' + userUid + fetchVar + requestId).once("value", function (snapshot) {
                                        let requestStored = snapshot.val();
                                        let senderName = requestStored.senderName;
                                        let histoCatchphrase = requestStored.histoCatchphrase;
                                        let catchPhrase = requestStored.catchphrase;
                                        let smileyType = userSendedPict;
                                        let profilPict = requestStored.senderProfilPict;
                                        let dateOfNotif = requestStored.date;
                                        if (waiting === "true") {
                                            createHisto(requestId, "toDo", senderName, histoCatchphrase, profilPict, dateOfNotif, "pending", smileyType, catchPhrase, requestId);
                                            firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).update({waiting: "false"});
                                        }
                                    });
                                    firebase.database().ref('user_profil/' + requestId + '/notificationReceived/').child(userUid).remove();
                                } else {
                                    if (waiting !== "true") {
                                        $(".target-notification").append('<div class="overlay-guest-no"><div class="overlay-guest-pictureEmoji"><figure class="overlay-profil-guest" style="background-image:url(' + profilPict + ')" ></figure> <figure class="emoji-send-guest"> <img src="' + userSendedPict + '" alt="Request No"/> </figure> </div> <div class="overlay-guest-title"> <p><span>' + userName + '</span></p> <p>a <span class="request-no">refusé</span> votre demande !</p> </div> </div>');
                                        $(".overlay-guest-no").show().animate({'top': '0px'}, 1000).delay(1000).fadeOut(1000, function () {
                                            $(this).remove();
                                        });
                                        Push.create('' + userName + ' a refusé le hush. ', {
                                            body: "Votre demande a été refusée.",
                                            icon: profilPict,
                                            link:path
                                        });
                                        firebase.database().ref('user_profil/' + requestId + fetchVar + userUid).once("value", function (snapshot) {

                                            let requestStored = snapshot.val();
                                            let senderName = requestStored.senderName;
                                            let histoCatchphrase = requestStored.histoCatchphrase;
                                            let catchPhrase = requestStored.catchphrase;
                                            let smileyType = userSendedPict;
                                            let profilPict = requestStored.senderProfilPict;
                                            let dateOfNotif = requestStored.date;
                                            createHisto(requestId, "done", senderName, histoCatchphrase, profilPict, dateOfNotif, "a refusé", smileyType, catchPhrase, requestId);
                                        });
                                    }
                                    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).update({status: "done"});
                                    setTimeout(function () {
                                        firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + requestId).set({status: "reset"});
                                        firebase.database().ref('user_profil/' + requestId + '/notificationReceived/').child(userUid).remove();
                                    }, lockedTimer)
                                }
                            }
                        });
                    });
                }
            });
        }
    });

    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationHistoric/done/').orderByChild("startedAt").on("child_added", function (snapshot) {
        let notifInfo = snapshot.val();
        let generatedId = notifInfo.startedAt;
        let catchPhrase = notifInfo.histoCatchphrase;
        let profilPict = notifInfo.profilPict;
        let emoji = notifInfo.notificationIs;
        let senderName = notifInfo.senderName;
        let status = notifInfo.status;
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let seconds = today.getSeconds();
        let minutes = today.getMinutes();
        let hour = today.getHours();
        let yyyy = today.getFullYear();
        let todarray = [seconds, minutes, hour, mm, dd, yyyy];
        let notifDate = notifInfo.dateOfNotif;
        let timestamp = [];
        timestamp[0] = todarray[0] - notifDate[0];
        timestamp[1] = todarray[1] - notifDate[1];
        timestamp[2] = todarray[2] - notifDate[2];
        timestamp[3] = todarray[3] - notifDate[3];
        timestamp[4] = todarray[4] - notifDate[4];
        timestamp[5] = todarray[5] - notifDate[5];

        let resultTime;

        if (timestamp[0] !== 0) {
            resultTime = "À l'instant";
            if (timestamp[1] > 0) {
                resultTime = "Il y a " + Math.abs(timestamp[1]) + " minutes ";
                if (timestamp[3] !== 0) {
                    resultTime = Math.abs(timestamp[3]) + " heures " + resultTime;
                    if (timestamp[2] !== 0) {
                        resultTime = Math.abs(timestamp[2]) + " jours " + resultTime;
                        if (timestamp[4] !== 0) {
                            resultTime = Math.abs(timestamp[4]) + " mois " + resultTime;
                            if (timestamp[5] !== 0) {
                                resultTime = Math.abs(timestamp[5]) + " ans " + resultTime;
                            }
                        }
                    }
                }
            }
        }
        if (document.getElementById(generatedId) === null) {
            $(".notifsPassed").prepend('<div id="' + generatedId + '" class="notifications-content"> <figure style="background: url(' + profilPict + ')"> </figure> <div class="notifications-content-informations"> <p class="notifications-name">' + senderName + ' </p> <p class="notifications-action">' + status + ' ' + catchPhrase + '</p> <p class="notifications-timeline">' + resultTime + '</p> </div> <img src="' + emoji + '" width="40" height="auto"> </div>');
        }
    });

    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationHistoric/toDo/').limitToLast(10).orderByChild("startedAt").on("child_added", function (snapshot) {
        $(".notifsPending").html("");
        let notifInfo = snapshot.val();
        let sender = notifInfo.senderId;
        let catchPhrase = notifInfo.catchphrase;
        let histoCatchphrase = notifInfo.histoCatchphrase;
        let profilPict = notifInfo.profilPict;
        let emoji = notifInfo.notificationIs;
        let senderName = notifInfo.senderName;
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let seconds = today.getSeconds();
        let minutes = today.getMinutes();
        let hour = today.getHours();
        let yyyy = today.getFullYear();
        let todarray = [seconds, minutes, hour, mm, dd, yyyy];
        let notifDate = notifInfo.dateOfNotif;
        let timestamp = [];
        timestamp[0] = todarray[0] - notifDate[0];
        timestamp[1] = todarray[1] - notifDate[1];
        timestamp[2] = todarray[2] - notifDate[2];
        timestamp[3] = todarray[3] - notifDate[3];
        timestamp[4] = todarray[4] - notifDate[4];
        timestamp[5] = todarray[5] - notifDate[5];

        let resultTime;

        if (timestamp[0] !== 0) {
            resultTime = "À l'instant";
            if (timestamp[1] > 0) {
                resultTime = "Il y a " + Math.abs(timestamp[1]) + " minutes ";
                if (timestamp[3] !== 0) {
                    resultTime = Math.abs(timestamp[3]) + " heures " + resultTime;
                    if (timestamp[2] !== 0) {
                        resultTime = Math.abs(timestamp[2]) + " jours " + resultTime;
                        if (timestamp[4] !== 0) {
                            resultTime = Math.abs(timestamp[4]) + " mois " + resultTime;
                            if (timestamp[5] !== 0) {
                                resultTime = Math.abs(timestamp[5]) + " ans " + resultTime;
                            }
                        }
                    }
                }
            }
        }
        let generatedFormattedId = "#" + sender;
        let validationId = generatedFormattedId + "yh";
        let negationId = generatedFormattedId + "nh";
        if (document.getElementById(sender + 'h') === null) {
            $(".notifsPending").prepend('<div id="' + sender + 'h" class="notifications-content"> <figure style="background: url(' + profilPict + ')"> </figure> <div class="notifications-content-informations"> <p class="notifications-name">' + senderName + ' </p> <p class="notifications-action">' + catchPhrase + '</p> <p class="notifications-timeline">' + resultTime + '</p> </div> <img src="' + emoji + '" width="40" height="auto"> <div class="toggleNotifications"> <div class="toggleNotifications-content"> <div id="' + sender + 'yh' + '" class="btn-cta-pink notif-yes">ACCEPTER</div> <div id="' + sender + 'nh' + '" class="btn-cta-black notif-no">REFUSER</div> </div> </div> </div>');
        }
        $(validationId).click(function () {
            $(this).parent().parent().remove();
            let waiting;
            firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).once('value').then(function (snapshot) {
                waiting = snapshot.val().waiting;
                if (waiting === "true") {
                    createHisto(sender, "done", senderName, histoCatchphrase, profilPict, notifDate, "a accepté", emoji, catchPhrase, sender);
                }
            });
            firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "keep"});
            $(generatedFormattedId + "h").remove();
            firebase.database().ref('user_profil/' + userUid + '/' + 'notificationSent/' + sender).update({waiting: "false"});
            let ref = snapshot.ref;
            ref.remove();
            // Now simply find the parent and return the name.
        });
        $(negationId).click(function () {
            $(this).parent().parent().remove();
            let waiting;
            firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).once('value').then(function (snapshot) {
                waiting = snapshot.val().waiting;
                if (waiting === "true") {
                    createHisto(sender, "done", senderName, histoCatchphrase, profilPict, notifDate, "a refusé", emoji, catchPhrase, sender);
                }
            });
            firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "refused"});
            $(generatedFormattedId + "h").remove();
            let ref = snapshot.ref;
            // Now simply find the parent and return the name.
            ref.remove();
        });
    });
    firebase.database().ref('user_profil/' + userUid + '/' + 'notificationHistoric/toDo/').on("value", function (snapshot) {
        let objectCount = 0;
        for (let count in snapshot.val()) {
            objectCount++;
        }
        if (objectCount !== 0) {
            $(".notif-ping").remove();
            $("#item-notifications").prepend('<div class="notif-ping">' + objectCount + '</div>');
        } else {
            $(".notif-ping").remove();
        }
    });
//
});
