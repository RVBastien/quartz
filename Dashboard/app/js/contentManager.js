$(function () {

    // Load tuto + co
    if (!cookieLib().userId || cookieLib().userId === null) {
        $(".app-container").load("login.html", function () {
            $("#loader").fadeOut();
        });
    } else {
        setTimeout(function () {
            $(".app-container").load("appContent.html", function () {
                $(".menu").fadeIn();
                $("#menu").load("menu.html");
                $("#menu").css('display', 'flex');
                $("#loader").fadeOut();
            });
        }, 2500);
    }
});