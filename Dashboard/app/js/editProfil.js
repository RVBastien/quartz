$(function () {
    $(".tab-profil input").prop('disabled', true);
    $(".tab-profil textarea").prop('disabled', true);
    $(".tab-profil select").prop('disabled', true);
    $(".editPen").click(function () {
        $(this).hide();
        $(".tab-profil input").prop('disabled', false);
        $(".tab-profil textarea").prop('disabled', false);
        $(".tab-profil select").prop('disabled', false);
        $(".submitCheck").fadeIn(200);
        $(".profilDescription").addClass("active");
        $(".profilStatus").addClass("active");
        $(".profilPict").addClass("active");
        $(".profilContent").addClass("active");
    });

    firebase.database().ref('user_profil/' + userUid).on('value', function (snapshot) {
        let userInfo = snapshot.val();
        $("#firstName").val(userInfo.firstName);
        $("#lastName").val(userInfo.lastName);
        $("#description").val(userInfo.description);
        document.getElementById("selectArrow").value = (userInfo.relationship);
        if (userInfo.ghostMod === "off") {
            $("#slideThree").prop("checked", true);
        } else {
            $("#slideThree").prop("checked", false);
        }
        $("#profil-pict").css("background", "url('" + (userInfo.profilPicture) + "')");
        if (userInfo.age === "") {
            $("#age").val("");
        } else {
            $("#age").val(userInfo.age);
        }

        let result = $('#result');
        let actionsNode = $('#actions');
        let currentFile;
        let coordinates;

        function updateResults(img) {
            let content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>')
            } else {
                content = $('<div>').append(img)
            }
            result.children().replaceWith(content);
            if (img.getContext) {
                actionsNode.show()
            }
        }

        function displayImage(file, options) {
            currentFile = file;
            if (!loadImage(
                    file,
                    updateResults,
                    options
                )) {
                result.children().replaceWith(
                    $('<span>' +
                        'Your browser does not support the URL or FileReader API.' +
                        '</span>')
                )
            }
        }

        function dropChangeHandler(e) {
            $(".edit-img").fadeIn();
            e.preventDefault();
            e = e.originalEvent;
            let target = e.dataTransfer || e.target;
            let file = target && target.files && target.files[0];
            let options = {
                maxWidth: 0.8 * ($("body").width()),
                canvas: true,
                pixelRatio: window.devicePixelRatio,
                downsamplingRatio: 2,
                orientation: true
            };
            if (!file) {
                return
            }
            displayImage(file, options);
        }

        // Hide URL/FileReader API requirement message in capable browsers:
        if (window.createObjectURL || window.URL || window.webkitURL ||
            window.FileReader) {
            result.children().hide()
        }

        $('#file-input')
            .on('change', dropChangeHandler);

        $("#cancel").click(function () {
            $(".edit-img").fadeOut();
            $("#actions").fadeOut();
        });
        $('#edit')
            .on('click', function (event) {
                event.preventDefault();
                let imgNode = result.find('img, canvas');
                let img = imgNode[0]
                let pixelRatio = window.devicePixelRatio || 1;
                imgNode.Jcrop({
                    setSelect: [
                        40,
                        40,
                        (img.width / pixelRatio) - 40,
                        (img.height / pixelRatio) - 40
                    ],
                    onSelect: function (coords) {
                        coordinates = coords
                    },
                    onRelease: function () {
                        coordinates = null
                    }
                }).parent().on('click', function (event) {
                    event.preventDefault()
                })
            });

        $('#crop')
            .on('click', function (event) {
                event.preventDefault();
                let img = result.find('img, canvas')[0];
                let pixelRatio = window.devicePixelRatio || 1;
                if (img && coordinates) {
                    updateResults(loadImage.scale(img, {
                        left: coordinates.x * pixelRatio,
                        top: coordinates.y * pixelRatio,
                        sourceWidth: coordinates.w * pixelRatio,
                        sourceHeight: coordinates.h * pixelRatio,
                        minWidth: result.width(),
                        maxWidth: 0.8 * ($("body").width()),
                        maxHeight: 0.8 * ($("body").width()),
                        pixelRatio: pixelRatio,
                        downsamplingRatio: 0.5
                    }));
                    coordinates = null
                }
            });
        $("#done").click(function () {
            let fileF = currentFile;

            // Create the file metadata
            let metadata = {
                contentType: 'image/jpeg'
            };
            let storageRef = firebase.storage().ref();
            // Upload file and metadata to the object 'images/mountains.jpg'
            let uploadTask = storageRef.child("image/" + fileF.name).put(fileF, metadata);

            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                function (snapshot) {
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                    }
                }, function (error) {

                    // A full list of error codes is available at
                    // https://firebase.google.com/docs/storage/web/handle-errors
                    switch (error.code) {
                        case 'storage/unauthorized':
                            // User doesn't have permission to access the object
                            break;

                        case 'storage/canceled':
                            // User canceled the upload
                            break;

                        case 'storage/unknown':
                            // Unknown error occurred, inspect error.serverResponse
                            break;
                    }
                }, function () {
                    // Upload completed successfully, now we can get the download URL
                    let downloadURL = uploadTask.snapshot.downloadURL;
                    firebase.database().ref('user_profil/' + userUid).update({
                        profilPicture: downloadURL
                    });
                    $(".edit-img").fadeOut();
                    $("#actions").fadeOut();
                    $("#profil-pict").css("transform","rotate(90deg)");
                });
        });
    });

    $(".submitCheck").click(function () {
        $(this).hide();
        $(".tab-profil input").prop('disabled', true);
        $(".tab-profil textarea").prop('disabled', true);
        $(".tab-profil select").prop('disabled', true);
        $(".editPen").fadeIn(200);
        $(".profilDescription").removeClass("active");
        $(".profilStatus").removeClass("active");
        $(".profilPict").removeClass('active');
        $(".profilContent").removeClass('active');
        let ghostMod;
        if($("#slideThree").prop("checked") === true){
            ghostMod = "off";
        }else{
            ghostMod = "on";
        }
        firebase.database().ref('user_profil/' + userUid).update({
            age: $("#age").val(),
            description:$("#description").val(),
            firstName:$("#firstName").val(),
            lastName:$("#lastName").val(),
            ghostMod:ghostMod,
            relationship:$("#selectArrow").val()
        });
    });

});
