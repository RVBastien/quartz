$(function () {
    firebase.database().ref('user_profil/' + userUid + '/' + 'friendList').on("value", function (snapshot) {
        for (let users in snapshot.val()) {
            firebase.database().ref('user_profil/' + userUid + '/' + 'friendList/' + users).once("value", function (snapshot) {
                if (snapshot.val() !== null) {
                    let targetedUserId = snapshot.val();
                    if (targetedUserId.notifStatus === "toDo") {
                        firebase.database().ref('user_profil/' + users).once("value", function (snapshot) {
                            let friendsInfo = snapshot.val();
                            Push.create('' + friendsInfo.firstName + ' vous a ajouté en ami !', {
                                body: "Vous avez un nouvel ami !",
                                icon: friendsInfo.profilPicture,
                                link:path
                            });
                        });
                        firebase.database().ref('user_profil/' + userUid + '/' + 'friendList/' + users).set({notifStatus: "done"})
                    }
                }
            });
            firebase.database().ref('user_profil/' + users).on("value", function (snapshot) {
                let userPict = snapshot.val().profilPicture;
                let userName = snapshot.val().firstName;
                let friendListId = users + "fl";
                if (document.getElementById(friendListId) === null) {
                    $(".friendPassed").prepend('<div id="' + friendListId + '" class="friendsContent"><div class="wrapper-profil"><figure style="background-image: url(' + userPict + ')";></figure> <div class="notifications-content-informations"> <p class="notifications-name">' + userName + '</p></div></div><div class="hushBtn"></div><ul class="emojisList"><li><img id="emoji-drink-3" class="emojiFriend" src="img/notification/drink.png" alt="EmojiDrink"></li><li><img id="emoji-dance-2" class="emojiFriend" src="img/notification/dance.png" alt="EmojiDance"></li><li><img id="emoji-smoke-1" class="emojiFriend" src="img/notification/smoke.png" alt="EmojiSmoke"></li></ul></div>');
                    let formattedTriger = "#"+friendListId + " .hushBtn";
                    let formattedTrigerBis = "#"+friendListId + " .emojisList";
                    $(formattedTriger).on('click', function () {
                        if ($(formattedTriger).hasClass("openedEmoji")) {
                            $(formattedTriger).removeClass("openedEmoji").animate({'right': '-80%'}, 400);
                            $(formattedTrigerBis).animate({'right': '-100%'}, 400);
                        } else {
                            $(formattedTriger).addClass("openedEmoji").animate({'right': '0%'}, 400);
                            $(formattedTrigerBis).animate({'right': '10%'}, 400);
                        }
                    });
                    let nodeSelector = "#" + friendListId;
                    let profilShowSelector = "#" + friendListId + " .wrapper-profil";
                    $(profilShowSelector).click(function () {
                        firebase.database().ref('user_profil/' + users).on("value", function (snapshot) {
                            let userPict = snapshot.val().profilPicture;
                            let userName = snapshot.val().firstName;
                            let lastName = snapshot.val().lastName;
                            let age = snapshot.val().age;
                            let description = snapshot.val().description;
                            let ghostMod = snapshot.val().ghostMod;
                            let userRelationShip = snapshot.val().relationship;
                            $(".profilPictGuest").css("background", "url('" + userPict + "')");
                            $("#firstNameGuest").html(userName);
                            $("#lastNameGuest").html(lastName);
                            $("#ageGuest").html(age);
                            $("#descriptionGuest").html(description);
                            $(".remove").show();
                            $(".add").hide();
                            if (ghostMod === "off") {
                                $(".profil-pop-in .profilStatus").show();
                                $(".relationStatusGuest").html(userRelationShip);
                            } else {
                                $(".profil-pop-in .profilStatus").hide();
                            }
                            $(".quit").click(function () {
                                $(".profil-pop-in").fadeOut();
                            });
                            $(".remove").attr("id", friendListId + "rm");
                            let removeId = "#" + friendListId + "rm";
                            $(removeId).click(function () {
                                firebase.database().ref('user_profil/' + userUid + '/' + 'friendList').child(users).remove();
                                $(nodeSelector).remove();
                                $(".profil-pop-in").fadeOut();
                            });
                            $(".profil-pop-in").fadeIn();
                        });
                    });
                    $("#emoji-drink-3").click(function () {
                        $(".hushBtn").removeClass("openedEmoji").animate({'right': '0'}, 400);
                        $('.emojisList').animate({'right': '-60%'}, 400);
                        let targetedUserId = users;
                        let userUid = cookieLib().userId;
                        return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
                            let sentNotification = snapshot.val() || {status: 'null'};
                            firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
                                let friendTest = snapshot.val();
                                if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                                    $('#overlay-notifs').css('display', 'none');
                                    // You anti Spam message goes here
                                    $('.toMuchHush').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush').fadeOut(1500);
                                        }, 1000);
                                    });
                                } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                                    $('.toMuchHush2').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush2').fadeOut(1500);
                                        }, 3000);
                                    });
                                } else {
                                    notificationManager(targetedUserId, userUid, "img/notification/drink.png", "Vous invite à boire", "de boire avec vous");
                                }
                            });
                        });
                    });
                    $("#emoji-dance-2").click(function () {
                        $(".hushBtn").removeClass("openedEmoji").animate({'right': '0'}, 400);
                        $('.emojisList').animate({'right': '-60%'}, 400);
                        let targetedUserId = users;
                        let userUid = cookieLib().userId;
                        return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
                            let sentNotification = snapshot.val() || {status: 'null'};
                            firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
                                let friendTest = snapshot.val();
                                if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                                    $('#overlay-notifs').css('display', 'none');
                                    // You anti Spam message goes here
                                    $('.toMuchHush').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush').fadeOut(1500);
                                        }, 1000);
                                    });
                                } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                                    $('.toMuchHush2').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush2').fadeOut(1500);
                                        }, 3000);
                                    });
                                } else {
                                    notificationManager(targetedUserId, userUid, "img/notification/dance.png", "Vous invite à danser", "de dansé avec vous");
                                }
                            });
                        });
                    });
                    $("#emoji-smoke-1").click(function () {
                        $(".hushBtn").removeClass("openedEmoji").animate({'right': '0'}, 400);
                        $('.emojisList').animate({'right': '-60%'}, 400);
                        let targetedUserId = users;
                        let userUid = cookieLib().userId;
                        return firebase.database().ref('user_profil/' + userUid + "/notificationSent" + '/' + targetedUserId).once('value').then(function (snapshot) {
                            let sentNotification = snapshot.val() || {status: 'null'};
                            firebase.database().ref('user_profil/' + userUid + "/friendList" + '/' + targetedUserId).once('value').then(function (snapshot) {
                                let friendTest = snapshot.val();
                                if (sentNotification.status && (sentNotification.status === "pending" || sentNotification.status === "seen")) {
                                    $('#overlay-notifs').css('display', 'none');
                                    // You anti Spam message goes here
                                    $('.toMuchHush').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush').fadeOut(1500);
                                        }, 1000);
                                    });
                                } else if (sentNotification.status === "refused" || sentNotification.status === "done" && friendTest === null) {
                                    $('.toMuchHush2').fadeIn(200, function () {
                                        setTimeout(function () {
                                            $('.toMuchHush2').fadeOut(1500);
                                        }, 3000);
                                    });
                                } else {
                                    notificationManager(targetedUserId, userUid, "img/notification/smoke.png", "Vous invite à fumer", "de fumé avec vous");
                                }
                            });
                        });
                    });
                }
            });
        }
    })
});
