// La documentation du carousel http://sorgalla.com/jcarousel/docs/

(function ($) {
    $(function () {
        let jcarousel = $('.jcarousel');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                $("a").click(function () {
                    if( $("#start").hasClass("target") === true ){
                        $(".jcarousel-control-next").html("Commencer")
                    }
                });
                let carousel = $(this),
                    width = $("body").width();
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                $(".jcarousel .slider-content").css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        jcarousel
        // Bind _before_ carousel initialization
            .on('jcarousel:targetin', 'li', function() {
                $(this).addClass('target');
            })
            .on('jcarousel:targetout', 'li', function() {
                $(this).removeClass('target');
            })
            .jcarousel({
                // Options go here
            });
        $('.jcarousel').jcarousel({
            scroll: 1,
        });

        $('.jcarousel').touchwipe({
            wipeLeft: function () {
                if( $("#start").hasClass("target") === true ){
                    $(".jcarousel-control-next").html("Commencer")
                }
                $('.jcarousel').jcarousel('scroll', '+=1');
            },
            min_move_x: 20,
            min_move_y: 20,
            preventDefaultEvents: false
        });


        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '3'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                $("a").click(function () {
                    if( $("#startwo").hasClass("target") === true ){
                        $(".jcarousel-control-next").html("Commencer")
                    }
                });
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);