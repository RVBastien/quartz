$(function () {
    let selectorConf = {
        facebookConnect: '.fb', // Trigger Facebook connect
        googleConnect: '.gc', // Trigger Google connect
    };

    //Param user
    let userId;
    let profilPictUrl = '';
    let first_name = '';
    let last_name = '';
    let birthday = '';
    let picture = '';
    let email = '';
    let gender = '';

    //non essential param
    let age = '';
    let work = '';
    let about = '';
    let education = '';


    //Calc age
    let today;
    let dob;


    // function of profil creation
    function profilCreator(userID, username, userMail, firstName, lastName, userAge, jobStatut, description, education, gender, profilPictUrl) {
        firebase.database().ref('user_profil/' + userID).set({
            userName: username,
            firstName: firstName,
            lastName: lastName,
            age: userAge,
            job: jobStatut,
            profilPicture: profilPictUrl,
            email: userMail,
            description: description,
            education: education,
            gender: gender,
            relationship:"Célibataire",
            ghostMod:"off"
        });
    }


    // function of profil check
    function checkUser(userId, nickname, email, first_name, last_name, age, work, about, education, gender, profilPictUrl) {
        let checkUser = firebase.database().ref('user_profil/' + userId);
        return checkUser.once('value').then(function (snapshot) {
            // We check if the user already exist
            if (snapshot.val() !== null) {
                document.cookie = "userUid = " + userId;
                document.cookie = "userFirstCo = false";
                setTimeout(function () {
                    $(".app-container").load("appContent.html",function () {
                        $(".menu").fadeIn();
                        $("#menu").load("menu.html");
                        $("#menu").css('display', 'flex');
                        $("#loader").fadeOut();
                        $(".deco").show();
                    });
                },2500);
            } else {
                document.cookie = "userUid = " + userId;
                document.cookie = "userFirstCo = true";
                profilCreator(userId, nickname, email, first_name, last_name, age, work, about, education, gender, profilPictUrl);
                $(".login").remove();
                setTimeout(function () {
                    $(".app-container").load("appContent.html",function () {
                        $(".menu").fadeIn();
                        $("#menu").load("menu.html");
                        $("#menu").css('display', 'flex');
                        $("#loader").fadeOut();
                        $(".deco").show();
                    });
                },2500);
            }
        });
    }

    // facebook api connexion
    window.fbAsyncInit = function () {
        FB.init({
            appId: '389516438109610',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v2.9'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        let js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/fr_FRA/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    // Connexion via facebook
    $(selectorConf.facebookConnect).click(function () {
        $("#loader").show();
        FB.login(function () {
            FB.api('/me', {fields: 'last_name,first_name,birthday,work,gender,education,email,picture.width(640),about'}, function (response) {
                if (response.birthday) {
                    birthday = response.birthday;
                    today = new Date(),
                        dob = new Date(birthday.replace(/(\d{2})[-/](\d{2})[-/](\d+)/, "$2/$1/$3"));
                    age = today.getFullYear() - dob.getFullYear();
                } else {
                    age = '';
                }
                if (response.email) {
                    email = response.email;
                }
                if (response.work) {
                    work = response.work;
                }
                if (response.about) {
                    about = response.about;
                }
                if (response.gender) {
                    gender = response.gender;
                }
                if (response.education) {
                    education = response.education;
                }

                last_name = response.last_name;
                first_name = response.first_name;
                userId = response.id;
                profilPictUrl = response.picture.data.url;

                // profil creation
                checkUser(userId, first_name, email, first_name, last_name, age, work, about, education, gender, profilPictUrl);
            });
        });
    });
});

