// Try HTML5 geolocation.
let userUid = cookieLib().userId;
let geocoder = new google.maps.Geocoder;

let isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if (isMobile.any()) {
//then we get geolocalisation data from google maps
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            let input = pos.lat + ',' + pos.lng;
            // we init our main function that locate the user and retrieve whoever is around
            geoUserAround(geocoder, input, userUid, false);
        });

        // we refresh that function until he disconnect
        setInterval(function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    let pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    let input = pos.lat + ',' + pos.lng;
                    document.cookie = "LatLng = " + input;
                    geoUserAround(geocoder, input, userUid, false);
                });
            }
        }, 10000);


        function arr_diff(a1, a2) {

            let a = [], diff = [];

            for (let i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
            }

            for (let i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                    delete a[a2[i]];
                } else {
                    a[a2[i]] = true;
                }
            }

            for (let k in a) {
                diff.push(k);
            }

            return diff;
        }

        function createLocation() {
            firebase.database().ref('location/').set({});
        }

        function createLocationCountry(country) {
            firebase.database().ref('location/' + country).set({});
        }

        function createLocationTown(country, town, exeption) {
            if (exeption === true) {
                firebase.database().ref('location/' + country + '/' + town + '/' + userUid + '/').set({
                    userUid: userUid
                });
            } else {
                firebase.database().ref('location/' + country + '/' + town + '/').set({});
            }

        }

        function createLocationDistrict(country, town, district) {
            firebase.database().ref('location/' + country + '/' + town + '/' + district + '/' + userUid + '/').set({
                userUid: userUid
            });
        }


        // Anti-Ghosting -> Comment that to test
        function locationCleaner(userLocation, userId, clearAll) {
            setTimeout(function () {
                let countryTab = [];
                let countryTown = [];
                let i = 0;
                let y = 0;
                let checkUser = firebase.database().ref('location/');
                return checkUser.on('value', function (snapshot) {
                    let locationObjet = snapshot.val();
                    for (let country in locationObjet) {
                        countryTab.push(country);
                        let checkUser = firebase.database().ref('location/' + country);
                        checkUser.on('value', function (snapshot) {
                            let locationObjet = snapshot.val();
                            for (let town in locationObjet) {
                                countryTown.push(town);
                                let checkUser = firebase.database().ref('location/' + countryTab[i] + '/' + town);
                                if (userLocation[2] !== undefined) {
                                    if (town === userLocation[1] && clearAll === false) {
                                    } else {
                                        checkUser.child(userId).remove();
                                    }
                                } else {
                                    if (town === userLocation[0] && clearAll === false) {
                                    } else {
                                        checkUser.child(userId).remove();
                                    }
                                }
                                checkUser.on('value', function (snapshot) {
                                    let locationObjet = snapshot.val();
                                    for (let district in locationObjet) {
                                        let checkUser = firebase.database().ref('location/' + countryTab[i] + '/' + countryTown[y] + '/' + district);
                                        if (district === userLocation[0] && clearAll === false) {
                                        } else {
                                            checkUser.child(userId).remove();
                                        }
                                    }
                                });
                                y++;
                            }
                        });
                        i++;
                    }
                });
            }, 10000);
        }

        function geoUserAround(geocoder, input, userId, clearAll) {
            let count = 0;
            let latlngStr = input.split(',', 2);
            let latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    if (results[1]) {
                        let location = (results[1]).formatted_address;
                        location = location.split(', ', 4);
                        if (location[3]) {
                            location = location.slice(1, 4);
                        }
                        let checkLocationTable = firebase.database().ref('location/');
                        checkLocationTable.once('value').then(function (snapshot) {
                            if (!snapshot) {
                                createLocation();
                            }
                        });
                        let checkLocation = firebase.database().ref('location/' + location[2]);
                        checkLocation.once('value').then(function (snapshot) {
                            if (snapshot.val() === undefined) {
                                createLocationCountry(location[2]);
                                createLocationTown(location[2], location[1]);
                                if (location[2]) {
                                    createLocationDistrict(location[2], location[1], location[0]);
                                } else {
                                    createLocationTown(location[1], location[0], true);
                                }
                            } else {
                                let checkLocation;
                                if (location[2]) {
                                    checkLocation = firebase.database().ref('location/' + location[2] + '/' + location[1] + '/' + location[0]);
                                } else {
                                    checkLocation = firebase.database().ref('location/' + location[1] + '/' + location[0] + '/');
                                }
                                checkLocation.once('value').then(function (snapshot) {
                                    if (snapshot.val() === undefined) {
                                        createLocationTown(location[2], location[1]);
                                        if (location[2]) {
                                            createLocationDistrict(location[2], location[1], location[0]);
                                        } else {
                                            createLocationTown(location[1], location[0], true);
                                        }
                                    } else {
                                        if (location[2]) {
                                            createLocationDistrict(location[2], location[1], location[0]);
                                            let arrayUserId = [];
                                            let arrayUserDisplayed = [];
                                            for (let userAround in snapshot.val()) {
                                                if (userAround !== userUid) {
                                                    arrayUserId.push(userAround);
                                                    checkLocation = firebase.database().ref('user_profil/' + userAround);
                                                    let displayedResult;
                                                    checkLocation.once('value').then(function (snapshot) {
                                                        count++;
                                                        let userInfo;
                                                        if (snapshot.val().age === "") {
                                                            userInfo = snapshot.val().firstName;
                                                        } else {
                                                            userInfo = snapshot.val().firstName + " | " + snapshot.val().age + " ans";
                                                        }
                                                        if (count === 1) {
                                                            $(".target_user-info").html(userInfo);
                                                            $("#home-picture").css("background-image", "url(" + snapshot.val().profilPicture + ")").attr("class", userAround);
                                                        }
                                                        displayedResult = "<div id='" + userAround + "' class='explorer-pict' value='" + userInfo + "'><figure><img src=" + snapshot.val().profilPicture + "></figure></div>";
                                                        if (document.getElementById(userAround) === null) {
                                                            $(".no-one").hide();
                                                            $(".users-displayed").append(displayedResult).css("white-space", "nowrap").css("font-size", "0");
                                                            $('.explorer-pict img').click(function () {
                                                                let clickedId = $(this).parent().parent().attr('id');
                                                                let src = $(this).attr('src');
                                                                let val = $(this).parent().parent().attr('value');
                                                                $(".target_user-info").html(val);
                                                                $("#home-picture").css("background-image", "url(" + src + ")").attr("class", clickedId);
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                            let i = ($(".explorer-pict").length) - 1;

                                            for (let y = 0; y <= i; y++) {
                                                arrayUserDisplayed.push($(".explorer-pict")[y].id);
                                            }
                                            let diff = arr_diff(arrayUserDisplayed, arrayUserId);
                                            for (let idToRemove of diff) {
                                                let formattedId = "#" + idToRemove;
                                                $(formattedId).remove();
                                            }
                                        } else {
                                            createLocationTown(location[1], location[0], true);
                                            let arrayUserId = [];
                                            let arrayUserDisplayed = [];
                                            for (let userAround in snapshot.val()) {
                                                if (userAround !== userUid) {
                                                    arrayUserId.push(userAround);
                                                    checkLocation = firebase.database().ref('user_profil/' + userAround);
                                                    let displayedResult;
                                                    checkLocation.once('value').then(function (snapshot) {
                                                        count++;
                                                        let userInfo;
                                                        if (snapshot.val().age === "") {
                                                            userInfo = snapshot.val().firstName;
                                                        } else {
                                                            userInfo = snapshot.val().firstName + " | " + snapshot.val().age + " ans";
                                                        }
                                                        if (count === 1) {
                                                            $(".target_user-info").html(userInfo);
                                                            $("#home-picture").css("background-image", "url(" + snapshot.val().profilPicture + ")").attr("class", userAround);
                                                        }
                                                        displayedResult = "<div id='" + userAround + "' class='explorer-pict' value='" + userInfo + "'><figure><img src=" + snapshot.val().profilPicture + "></figure></div>";
                                                        if (document.getElementById(userAround) === null) {
                                                            $(".no-one").hide();
                                                            $(".users-displayed").append(displayedResult).css("white-space", "nowrap").css("font-size", "0");
                                                            $('.explorer-pict img').click(function () {
                                                                let clickedId = $(this).parent().parent().attr('id');
                                                                let src = $(this).attr('src');
                                                                let val = $(this).parent().parent().attr('value');
                                                                $(".target_user-info").html(val);
                                                                $("#home-picture").css("background-image", "url(" + src + ")").attr("class", clickedId);
                                                            });
                                                        }
                                                    });
                                                }
                                            }

                                            let i = ($(".explorer-pict").length) - 1;

                                            for (let y = 0; y <= i; y++) {
                                                arrayUserDisplayed.push($(".explorer-pict")[y].id);
                                            }
                                            let diff = arr_diff(arrayUserDisplayed, arrayUserId);
                                            for (let idToRemove of diff) {
                                                let formattedId = "#" + idToRemove;
                                                $(formattedId).remove();
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        locationCleaner(location, userId, clearAll);
                    } else {
                        window.alert('No results found');
                    }
                }
            });
        }

        $(".deco").click(function () {
            document.cookie = "userUid = ";
            FB.logout(function (response) {
                // user is now logged out
            });
            geoUserAround(geocoder, cookieLib().userLatLng, userUid, true);
            window.close();
        });

    }
}else{
    $(".no-one p").html("Vous êtes sur votre ordinateur la géolocalisation est donc désactivé, mais vous pouvez toujours inviter vos amis et gérer votre profil :)")
}
