/*** OVERLAY ***/
$(function () {
    $(".confirmation").hide();
    $("form").submit(function (e) {
        e.preventDefault();
        $.post("mailChimpAutomation.php",
            {
                email: $("#sendedEmail").val()
            });
        $("#sendedEmail").val("");
        $(".confirmation").fadeIn(1000, function () {
            $(this).fadeOut(1000);
        })
    });

    $(".btn-cta-hush").on('click', function () {
        if (document.getElementsByClassName("null").length === 0) {
            let userImg = ($("#home-picture").css('background-image'));
            $("#overlay-profil figure").css("background-image", userImg);
            let userName = $(".target_user-info").html();
            $("#overlay-profil-name").html(userName);
            $("#profil-name-send").html(userName);
            $("#overlay-send").fadeToggle('slow').css('display', 'block');
            $("#carousel").toggleClass('fixed');
            if ($(".btn-cta-hush-overlay").show()) {
                $('#emoji-1').addClass('emoji-anim');
                $('#emoji-2').addClass('emoji-anim');
                $('#emoji-3').addClass('emoji-anim');
            }
        }
    });

    $(".btn-cta-hush-overlay").on('click', function () {
        $('#overlay-send').fadeOut(200);
    });
    $("#overlay-close").on('click', function () {
        $('#overlay-send').fadeOut(200);
    });
    $('#emoji-3').on('click', function () {
        $("#overlay-send").css('display', 'none');
        $("#overlay-notifs").css('display', 'block').delay(2000).fadeOut(800);
        $("#emojiSmoke").hide();
        $("#emojiDance").hide();
        $("#emojiDrink").fadeIn();
    });
    $('#emoji-1').on('click', function () {
        $("#overlay-send").css('display', 'none');
        $("#overlay-notifs").css('display', 'block').delay(2000).fadeOut(800);
        $("#emojiDance").hide();
        $("#emojiDrink").hide();
        $("#emojiSmoke").fadeIn();
    });
    $('#emoji-2').on('click', function () {
        $("#overlay-send").css('display', 'none');
        $("#overlay-notifs").css('display', 'block').delay(2000).fadeOut(800);
        $("#emojiDrink").hide();
        $("#emojiSmoke").hide();
        $("#emojiDance").fadeIn();
    });
    let heightHome = $("#home").height()-30;
    let heightMenu = $("#menu").height();
    $(".tab").css("height", "calc( 100% - " + heightMenu + "px");
    let totalSpace = heightHome + heightMenu + "px";
    $(".users-displayed").css("height", "calc( 100% - " + totalSpace + "");
});
