$(function () {
    $(".menu-item").click(function () {
        $(".menu-item").removeClass("active");
        $(this).addClass("active");
        let itemId = $(this).attr('id');
        itemId = itemId.substr(5);
        let tabClass = ".tab-";
        tabClass = tabClass + itemId;
        $(tabClass).fadeIn();
        document.body.style.overflow = 'hidden';
        $(".tab:not(" + tabClass + ")").fadeOut();
    });

    $("#item-friends").click(function () {
        $(this).removeClass("superActive");
    });

    // Refresh notif

    $("#item-notifications").click(function () {
        $(".notifsPending").html("");
        $(".notifsPassed").html("");
        firebase.database().ref('user_profil/' + userUid + '/' + 'notificationHistoric/done/').limitToLast(10).orderByChild("startedAt").on("child_added", function (snapshot) {
            let notifInfo = snapshot.val();
            let generatedId = notifInfo.startedAt;
            let catchPhrase = notifInfo.histoCatchphrase;
            let profilPict = notifInfo.profilPict;
            let emoji = notifInfo.notificationIs;
            let senderName = notifInfo.senderName;
            let status = notifInfo.status;
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth() + 1; //January is 0!
            let seconds = today.getSeconds();
            let minutes = today.getMinutes();
            let hour = today.getHours();
            let yyyy = today.getFullYear();
            let todarray = [seconds, minutes, hour, mm, dd, yyyy];
            let notifDate = notifInfo.dateOfNotif;
            let timestamp = [];
            timestamp[0] = todarray[0] - notifDate[0];
            timestamp[1] = todarray[1] - notifDate[1];
            timestamp[2] = todarray[2] - notifDate[2];
            timestamp[3] = todarray[3] - notifDate[3];
            timestamp[4] = todarray[4] - notifDate[4];
            timestamp[5] = todarray[5] - notifDate[5];

            let resultTime;

            if (timestamp[0] !== 0) {
                resultTime = "À l'instant";
                if (timestamp[1] > 0) {
                    resultTime = "Il y a " + Math.abs(timestamp[1]) + " minutes ";
                    if (timestamp[3] !== 0) {
                        resultTime = Math.abs(timestamp[3]) + " heures " + resultTime;
                        if (timestamp[2] !== 0) {
                            resultTime = Math.abs(timestamp[2]) + " jours " + resultTime;
                            if (timestamp[4] !== 0) {
                                resultTime = Math.abs(timestamp[4]) + " mois " + resultTime;
                                if (timestamp[5] !== 0) {
                                    resultTime = Math.abs(timestamp[5]) + " ans " + resultTime;
                                }
                            }
                        }
                    }
                }
            }
            if (document.getElementById(generatedId) === null) {
                $(".notifsPassed").prepend('<div id="' + generatedId + '" class="notifications-content"> <figure style="background: url(' + profilPict + ')"> </figure> <div class="notifications-content-informations"> <p class="notifications-name">' + senderName + ' </p> <p class="notifications-action">' + status + ' ' + catchPhrase + '</p> <p class="notifications-timeline">' + resultTime + '</p> </div> <img src="' + emoji + '" width="40" height="auto"> </div>');
            }
        });
        firebase.database().ref('user_profil/' + userUid + '/' + 'notificationHistoric/toDo/').orderByChild("startedAt").on("child_added", function (snapshot) {
            $(".notifsPending").html("");
            let notifInfo = snapshot.val();
            let sender = notifInfo.senderId;
            let catchPhrase = notifInfo.catchphrase;
            let histoCatchphrase = notifInfo.histoCatchphrase;
            let profilPict = notifInfo.profilPict;
            let emoji = notifInfo.notificationIs;
            let senderName = notifInfo.senderName;
            let status = notifInfo.status;
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth() + 1; //January is 0!
            let seconds = today.getSeconds();
            let minutes = today.getMinutes();
            let hour = today.getHours();
            let yyyy = today.getFullYear();
            let todarray = [seconds, minutes, hour, mm, dd, yyyy];
            let notifDate = notifInfo.dateOfNotif;
            let timestamp = [];
            timestamp[0] = todarray[0] - notifDate[0];
            timestamp[1] = todarray[1] - notifDate[1];
            timestamp[2] = todarray[2] - notifDate[2];
            timestamp[3] = todarray[3] - notifDate[3];
            timestamp[4] = todarray[4] - notifDate[4];
            timestamp[5] = todarray[5] - notifDate[5];

            let resultTime;

            if (timestamp[0] !== 0) {
                resultTime = "À l'instant";
                if (timestamp[1] > 0) {
                    resultTime = "Il y a " + Math.abs(timestamp[1]) + " minutes ";
                    if (timestamp[3] !== 0) {
                        resultTime = Math.abs(timestamp[3]) + " heures " + resultTime;
                        if (timestamp[2] !== 0) {
                            resultTime = Math.abs(timestamp[2]) + " jours " + resultTime;
                            if (timestamp[4] !== 0) {
                                resultTime = Math.abs(timestamp[4]) + " mois " + resultTime;
                                if (timestamp[5] !== 0) {
                                    resultTime = Math.abs(timestamp[5]) + " ans " + resultTime;
                                }
                            }
                        }
                    }
                }
            }
            let generatedFormattedId = "#" + sender;
            let validationId = generatedFormattedId + "yh";
            let negationId = generatedFormattedId + "nh";
            if (document.getElementById(sender + 'h') === null) {
                $(".notifsPending").prepend('<div id="' + sender + 'h" class="notifications-content"> <figure style="background: url(' + profilPict + ')"> </figure> <div class="notifications-content-informations"> <p class="notifications-name">' + senderName + ' </p> <p class="notifications-action">' + catchPhrase + '</p> <p class="notifications-timeline">' + resultTime + '</p> </div> <img src="' + emoji + '" width="40" height="auto"> <div class="toggleNotifications"> <div class="toggleNotifications-content"> <div id="' + sender + 'yh' + '" class="btn-cta-pink notif-yes">ACCEPTER</div> <div id="' + sender + 'nh' + '" class="btn-cta-black notif-no">REFUSER</div> </div> </div> </div>');
                $(validationId).click(function () {
                    let waiting;
                    $(this).parent().parent().remove();
                    firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "keep"});
                    $(generatedFormattedId + "h").remove();
                    firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).once('value').then(function (snapshot) {
                        waiting = snapshot.val().waiting;
                        if (waiting === "true") {
                            createHisto(sender, "done", senderName, histoCatchphrase, profilPict, notifDate, "a accepté", emoji, catchPhrase, sender);
                        }
                    });
                    let ref = snapshot.ref;
                    ref.remove();
                    // Now simply find the parent and return the name.
                });
                $(negationId).click(function () {
                    $(this).parent().parent().remove();
                    firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).update({status: "refused"});
                    $(generatedFormattedId + "h").remove();
                    let waiting;
                    firebase.database().ref('user_profil/' + sender + "/notificationSent/" + userUid).once('value').then(function (snapshot) {
                        waiting = snapshot.val().waiting;
                        if (waiting === "true") {
                            createHisto(sender, "done", senderName, histoCatchphrase, profilPict, notifDate, "a refusé", emoji, catchPhrase, sender);
                        }
                    });
                    let ref = snapshot.ref;
                    // Now simply find the parent and return the name.
                    ref.remove();
                });
            }
        });
//
    });
});
