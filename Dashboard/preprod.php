<!DOCTYPE html>
<html lang="en">
<head>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <title>Preprod</title>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.1.2/firebase.js"></script>
<link href="css/style.css" rel="stylesheet"/>
<?php if ($_SESSION['login'] == true) { ?>
    <header>
        <nav>
            <ul>
                <li><a href="dashboard.html">Home</a></li>
                <li><a href="preprod.php">Preproduction</a></li>
            </ul>
        </nav>
    </header>
    <div class="project"><a href="landing">Landing</a></div>
    <div class="project"><a href="app">HushApp</a></div>
<?php } else { ?>
    <div class="youShallNotPass">
    </div>
<?php } ?>
</body>
</html>