// La documentation du carousel http://sorgalla.com/jcarousel/docs/
(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = $(".section-4").width();
                if ($("#team")) {
                    width = $("#team").width();
                }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                $(".jcarousel .slider-content").css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel').jcarousel({
            scroll: 1,
        });

        $('.jcarousel').touchwipe({
            wipeLeft: function () {
                $('.jcarousel').jcarousel('scroll', '+=1');
            },
            wipeRight: function () {
                $('.jcarousel').jcarousel('scroll', '-=1');
            },
            min_move_x: 20,
            min_move_y: 20,
            preventDefaultEvents: false
        });

        // Desactivation du scroll pour les mobiles

        var mobileCheck = false;

        if (window.matchMedia("(min-width: 950px)").matches) {
            mobileCheck = true;
        }

        $('.jcarousel').jcarouselAutoscroll({
            interval: 500000, // Changer l'interval entre deux slides
            autostart: mobileCheck //( pour couper le lancement automatique du slider )
        });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });

        $('#second .jcarousel').jcarousel();

        $('#second .jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('#second .jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        $('#second .jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();

    $('.jcarousel #second').touchwipe({
        wipeLeft: function () {
            $('.jcarousel #second').jcarousel('scroll', '+=1');
        },
        wipeRight: function () {
            $('.jcarousel #second').jcarousel('scroll', '-=1');
        },
        min_move_x: 20,
        min_move_y: 20,
        preventDefaultEvents: false
    });
})(jQuery);