$(".connect-triger").click(function () {
    $(".social-connect-container").fadeIn();
    $(".leave").click(function () {
        $(".social-connect-container").fadeOut();
    });
});

firebase.auth().onAuthStateChanged(function (user) {

    /* Limitation de l'usage du bouton de connection -> Connection -> Récupération des données utilisateurs -> Message de bienvenue */
    let debounce = true;

    let subscribeCounter;

    function dataOriginLocator(landingType, subscribeCounter) {
        firebase.database().ref('/subscribeFrom_' + landingType).set({
            subscribeCounter: subscribeCounter
        });
    };


    if (debounce) {
        $("#signInG").click(function () {
            let provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function (result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                let token = result.credential.accessToken;
                // The signed-in user info.
                let user = result.user;
                $.post("mailChimpAutomation.php",
                    {
                        email: user.email,
                        name: user.displayName,
                    });
                $(".co-container").html("Votre demande a bien été prise en compte.");
                $(".social-connect-container").fadeOut(4000);
                debounce = false;
                setTimeout(function () {
                    debounce = true;
                }, 1000);
                // ...
                return firebase.database().ref('/subscribeFrom_' + landingType).once('value').then(function (snapshot) {
                    visitNumberData = parseInt(snapshot.val().subscribeCounter);
                    if (visitNumberData === undefined) {
                        dataOriginLocator(landingType, 1);
                    } else {
                        visitNumberData++;
                        dataOriginLocator(landingType, visitNumberData);
                    }
                });
            }).catch(function (error) {
                // Handle Errors here.
                let errorCode = error.code;
                let errorMessage = error.message;
                // The email of the user's account used.
                let email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                let credential = error.credential;
                // ...
                console.log(errorMessage);
            });

        });
        $("#signInFb").click(function () {
            let provider = new firebase.auth.FacebookAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function (result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                let token = result.credential.accessToken;
                // The signed-in user info.
                let user = result.user;
                $.post("mailChimpAutomation.php",
                    {
                        email: user.email,
                        name: user.displayName,
                    });
                $(".co-container").html("Votre demande a bien été prise en compte.");
                $(".social-connect-container").fadeOut(4000);
                debounce = false;
                setTimeout(function () {
                    debounce = true;
                }, 1000);
                // ...
            }).catch(function (error) {
                // Handle Errors here.
                let errorCode = error.code;
                let errorMessage = error.message;
                // The email of the user's account used.
                let email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                let credential = error.credential;
                // ...
                console.log(errorMessage);
                return firebase.database().ref('/subscribeFrom_' + landingType).once('value').then(function (snapshot) {
                    visitNumberData = parseInt(snapshot.val().subscribeCounter);
                    if (visitNumberData === undefined) {
                        dataOriginLocator(landingType, 1);
                    } else {
                        visitNumberData++;
                        dataOriginLocator(landingType, visitNumberData);
                    }
                });
            });
        });
    }


});