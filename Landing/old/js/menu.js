$(function () {
    $(window).scroll(function () {
        posScroll = jQuery(document).scrollTop();
        if (posScroll >= 100) {
            $('header').addClass('nav-background').show(2500);
        }
        else {
            $('header').removeClass('nav-background').css('display','flex');
        }
    });
});
let menu = $('#menu')[0].innerHTML;
$(function () {
    $("#burger-menu").on('click', function () {
        $(this).toggleClass('clicked');
        console.log($("#menu"));
        $("#overlay-menu").fadeToggle().html(menu).css('display','flex');
    });
});