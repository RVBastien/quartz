// La documentation du carousel http://sorgalla.com/jcarousel/docs/
(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = $(".section-4").width();


                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                $(".jcarousel .slider-content").css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel').jcarousel({
            scroll: 1,
        });

        

        // Desactivation du scroll pour les mobiles

        var mobileCheck = false;

        if (window.matchMedia("(min-width: 950px)").matches) {
            mobileCheck = true;
        }

        $('.jcarousel').jcarouselAutoscroll({
            interval: 500000, // Changer l'interval entre deux slides
            autostart: mobileCheck //( pour couper le lancement automatique du slider )
        });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);