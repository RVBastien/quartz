/**
 * Created by Floriane on 30/06/2017.
 */
$(function(){
   $(".teamPuseld").on('click', function(){
       $(".teamPuseld").removeClass('member');
       $('.teamOverlay').hide();
      $(this).addClass('member');
       if( $(".teamPuseld").hasClass('member') ){
           $(this).find(".teamOverlay").css('display','flex');
       }
   });
});