$(function () {
    $(window).scroll(function () {
        posScroll = jQuery(document).scrollTop();
        if (posScroll >= 100) {
            $('header').addClass('nav-background').show(1500);
        }
        else {
            $('header').removeClass('nav-background').css('display', 'flex');
        }
    });
    let menu = $('#menu')[0].innerHTML;
    $("#burger-menu").on('click', function () {
        $(this).toggleClass('clicked');
        console.log($("#menu"));
        $("#overlay-menu").fadeToggle().html(menu).css('display', 'flex');
    });

    $('a[href^="#"]').on('click', function (event) {

        let target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });

    $("form").submit(function () {
        $.post("mailChimpAutomation.php",
            {
                email: $("#email").val()
            });
    });

});